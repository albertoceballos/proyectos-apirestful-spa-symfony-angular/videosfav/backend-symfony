<?php

namespace App\Services;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//JSON web tokens
use Firebase\JWT\JWT;
//modelo
use App\Entity\User;

class JwtAuth extends AbstractController {

    public $key;

    public function __construct() {

        $this->key = 'una_clave_secreta99900';
    }

    public function signup($email, $password, $getToken = null) {

        //Comprobar en la BBDD el user y email
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['email' => $email, 'password' => $password]);

        $signup = false;

        //si el login es correcto
        if (is_object($user)) {
            $signup = true;
        }


        if ($signup) {

            //preparar array para el token
            $token = [
                'sub' => $user->getId(),
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'email' => $user->getEmail(),
                'role' => $user->getRole(),
                'iat' => time(),
                'exp' => time() + (60 * 60 * 24 * 7),
                'avatar'=>$user->getAvatar(),
            ];

            //codificar token
            $jwt = JWT::encode($token, $this->key, 'HS256');

            //devolver token o datos decodificados según enviamos o no $getToken
            if (!empty($getToken)) {
                $data = $jwt;
            } else {
                $decoded = JWT::decode($jwt, $this->key, ['HS256']);
                $data = $decoded;
            }
            $data = [
                'code' => '200',
                'status' => 'success',
                'message' => 'Login correcto',
                'data' => $data,
            ];
        } else {
            $data = [
                'code' => '404',
                'status' => 'error',
                'message' => 'Login incorrecto',
            ];
        }

        return $data;
    }

    public function checkToken($jwt,$identity=false) {

        $auth = false;

        try {
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $exc) {
            $auth = false;
        } catch (\DomainException $exc) {
            $auth = false;
        }

        if (!empty($decoded)) {
            $auth = true;
            
        }
        
        if($identity!=false && $auth==true){
                
                $auth=$decoded;
        }


        return $auth;
    }

}
