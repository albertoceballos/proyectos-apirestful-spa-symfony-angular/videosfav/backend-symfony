<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//componente para respuestas en formato JSON
use Symfony\Component\HttpFoundation\JsonResponse;
//componente para validaciones
use Symfony\Component\Validator\Validation;
//Constraints de validaciones
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
//servicio
use App\Services\JwtAuth;
use App\Entity\User;
use App\Entity\Video;

class UserController extends AbstractController {

    //método para serializar los datos 
    private function resjson($data) {
        //serializar datos que llegan al método
        
        //de este modo me serializa todos los objetos que tenga anidados el primer objeto que serializo
        $encoders = [new JsonEncoder()]; // If no need for XmlEncoder
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        // Serialize your object in Json
        $jsonObject = $serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        //Response con HttpFoundation
        $response = new Response();

        //setear contenido:
        $response->setContent($jsonObject);

        //setear cabceras:
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function index() {
        
        $data=[
           'status'=>'error',
           'code'=>400,
           'message' =>'No se encontraron usuarios',
        ];

        $user_repo = $this->getDoctrine()->getRepository(User::class);
        $users = $user_repo->findAll();
        
        if(!empty($users)){
            $data=[
                'status'=>'success',
                'code'=>200,
                'users'=>$users,
            ];
        }

        return $this->resjson($data);

    }

    public function register(Request $request) {

        //recoger información que llega por POST
        $json = $request->get('json', null);
        $params = json_decode($json);

        //datos por defecto
        $data = [
            'status' => 'error',
            'code' => 200,
            'message' => 'Usuario no registrado',
        ];

        //comprobar datos
        if ($json != null) {
            $name = (!empty($params->name)) ? $params->name : null;
            $surname = (!empty($params->surname)) ? $params->surname : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;

            $validator = Validation::createValidator();

            //validamos  para que el email sea un email válido
            $email_validation = $validator->validate($email, [
                new Email(),
            ]);

            //validamos la contraseña para que tenga un mínimo de 5 caracteres
            $password_validation = $validator->validate($password, [
                new Length(['min' => 5]),
            ]);

            if (!empty($email) && count($email_validation) == 0 && (!empty($password)) && count($password_validation) == 0 && (!empty($name))) {

                //crear nuevo usuario
                $user = new User();
                $user->setName($name);
                $user->setSurname($surname);
                $user->setEmail($email);
                $user->setRole('ROLE_USER');
                $user->setCreatedAt(new \DateTime('now'));

                //cifrar la contraseña
                $pwd = hash('sha256', $password);
                $user->setPassword($pwd);

                //comprobar si hay usuarios duplicados registrados
                $user_repo = $this->getDoctrine()->getRepository(User::class);

                $isset_email = $user_repo->findOneBy(['email' => $email]);

                if (empty($isset_email)) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $save = $em->flush();

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Usuario registrado con éxito',
                        'user' => $user,
                    ];
                } else {
                    $data = [
                        'status' => 'error',
                        'code' => 404,
                        'message' => 'El usuario ya existe',
                    ];
                }
            } else {
                $data = [
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'Validación incorrecta',
                ];
            }
        }

        return new JsonResponse($data);
    }

    public function login(Request $request, JwtAuth $JwtAuthService) {

        //recoger parámetros por POST
        $json = $request->get('json', null);
        $params = json_decode($json);

        //Datos por defecto
        $data = [
            'code' => 200,
            'status' => 'error',
            'message' => "El usuario no se ha podido identificar",
        ];

        //comprobar datos
        if ($json != null) {
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;
            $getToken = (!empty($params->getToken)) ? $params->getToken : null;

            //validación del E-mail
            $validator = Validation::createValidator();
            $email_validator = $validator->validate($email, [
                new Email(),
            ]);

            //Comprobar que todo es válido
            if (!empty($email) && count($email_validator) == 0 && !empty($password)) {
                //cifrar contraseña
                $pwd = hash('sha256', $password);

                if ($getToken) {
                    $signup = $JwtAuthService->signup($email, $pwd, $getToken);
                } else {
                    $signup = $JwtAuthService->signup($email, $pwd);
                }

                return new JsonResponse($signup);
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Validación incorrecta',
                ];
            }
        }
        return new JsonResponse($data);
    }

    public function edit(Request $request, JwtAuth $jwauthService) {
        //recoger token de la cabecera
        $token = $request->headers->get('Authorization');

        //datos a devolver por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'Usuario no actualizado',
        ];

        //lamar al método del servicio para comprobar si el token es correcto
        $authCheck = $jwauthService->checkToken($token);

        //si eel token es correcto
        if ($authCheck != false) {

            //sacar identidad del usuario con el token
            $identity = $jwauthService->checkToken($token, true);

            $em = $this->getDoctrine()->getManager();

            //sacar el objeto de usuario completo de la BBDD
            $user_repo = $em->getRepository(User::class);
            $user = $user_repo->find($identity->sub);

            //Recoger datos para actualizar de POST
            $json = $request->get('json', null);
            $params = json_decode($json);

            //validar datos
            if ($json != null) {

                $name = !empty($params->name) ? $params->name : null;
                $surname = !empty($params->surname) ? $params->surname : null;
                $email = !empty($params->email) ? $params->email : null;

                //validación del email
                $validator = Validation::createValidator();
                $email_validator = $validator->validate($email, [
                    new Email(),
                ]);

                //si todo es coorecto
                if ((!empty($name)) && (!empty($email)) && count($email_validator) == 0) {

                    //setear nuevos valores al objeto user
                    $user->setName($name);
                    $user->setSurname($surname);
                    $user->setEmail($email);

                    //comprobar duplicados
                    $isset_user = $user_repo->findBy(['email' => $email]);

                    //si no encuentra duplicado el email , guardamos
                    if (count($isset_user) == 0 || $email == $identity->email) {
                        $em->persist($user);
                        $em->flush();
                        $data = [
                            'code' => 200,
                            'status' => 'success',
                            'message' => 'Usuario actualizado',
                            'user' => $user,
                        ];
                    } else {

                        $data = [
                            'code' => 400,
                            'status' => 'error',
                            'message' => 'El email de usuario ya existe',
                        ];
                    }
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'La validación de los datos no es correcta',
                    ];
                }
            }
        }

        return new JsonResponse($data);
    }

    public function uploadImage(Request $request, JwtAuth $jwtauthService) {

        //recoder token
        $token = $request->headers->get('Authorization');

        $authCheck = $jwtauthService->checkToken($token);

        //si el token es correcto
        if ($authCheck != false) {
            //sacar identidad
            $identity = $jwtauthService->checkToken($token, true);

            $em = $this->getDoctrine()->getManager();
            $user_repo = $this->getDoctrine()->getRepository(User::class);
            $user = $user_repo->find($identity->sub);

            //conseguir datos imagen
            $file = $request->files->get('file0');

            //si no está vacia la imagen
            if (!empty($file)) {
                $ext = $file->guessExtension();

                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif' || $ext == 'png') {
                    //dar nuevo nombre a la imagen
                    $file_name = $user->getId() . time() . "." . $ext;
                    //subir imagen
                    $file->move('uploads/users', $file_name);
                    $user->setAvatar($file_name);

                    //persistir user y guardar en la BBDD
                    $em->persist($user);
                    $em->flush();
                }
            }

            $data = [
                'file_name' => $file_name,
                'ext' => $ext,
                'user' => $user,
            ];
        }
        return new JsonResponse($data);
    }

}
