<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//componente para respuestas en formato JSON
use Symfony\Component\HttpFoundation\JsonResponse;
//componente para validaciones
use Symfony\Component\Validator\Validation;
//Constraints de validaciones
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
//servicio
use App\Services\JwtAuth;
use App\Entity\User;
use App\Entity\Video;
//paginador
use Knp\Component\Pager\PaginatorInterface;

class VideoController extends AbstractController {

    public function index() {
        return $this->json([
                    'message' => 'Welcome to your new controller!',
                    'path' => 'src/Controller/VideoController.php',
        ]);
    }

    private function resjson($data) {
        //serializar datos que llegan al método
        $json = $this->get('serializer')->serialize($data, 'json');

        //Response con HttpFoundation
        $response = new Response();

        //setear contenido:
        $response->setContent($json);

        //setear cabceras:
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    //Publicar nuevo vídeo
    public function newVideo(Request $request, JwtAuth $JwtauthService) {

        //array de datos por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'El vídeo no ha podido crearse',
        ];

        //recoger el token de la cabecera
        $token = $request->headers->get('Authorization', null);
        //comprobar si es correcto:
        $authCheck = $JwtauthService->checkToken($token);

        //si es correcto
        if ($authCheck != false) {
            //saco la identidad del usuario del token
            $identity = $JwtauthService->checkToken($token, true);

            //recojo datos del video desde POST
            $json = $request->get('json');
            $params = json_decode($json);

            //validación de datos
            $user_id = $identity->sub;
            $title = !empty($params->title) ? $params->title : null;
            $description = !empty($params->description) ? $params->description : null;
            $url = !empty($params->url) ? $params->url : null;

            //si los datos son correctos
            if (!empty($user_id) && !empty($title) && !empty($url)) {

                //sacar todo el objeto de usuario del usuario identificado
                $em = $this->getDoctrine()->getManager();
                $user_repo = $this->getDoctrine()->getRepository(User::class);
                $user = $user_repo->find($user_id);

                //Crear y setear el nuevo objeto video
                $video = new Video();
                $video->setUser($user);
                $video->setTitle($title);
                $video->setDescription($description);
                $video->setUrl($url);
                $video->setStatus('normal');
                $video->setCreatedAt(new \DateTime('now'));
                $video->setUpdatedAt(new \DateTime('now'));
                //persistir y guardar
                $em->persist($video);
                $em->flush();

                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Nuevo vídeo guradado con éxito',
                    'video' => $video,
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'La validación de datos no es correcta',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'La autenticación no es válida',
            ];
        }
        return $this->resjson($data);
    }

    //Listar videos del usuario logueado
    public function videos(Request $request, JwtAuth $jwauthService, PaginatorInterface $paginator) {

        //datos por defecto
        $data = [
            'code' => 200,
            'status' => 'error',
            'message' => '',
        ];

        //recoger autenticación de la cabecera
        $token = $request->headers->get('Authorization', null);

        //comprobar token
        $authCheck = $jwauthService->checkToken($token);

        //si es válido
        if ($authCheck != false) {
            //recogemos la identidad de usuario
            $identity = $jwauthService->checkToken($token, true);
            $user_id = $identity->sub;

            $em = $this->getDoctrine()->getManager();
            $user_repo = $this->getDoctrine()->getRepository(User::class);

            //consulta (en DQL)
            $dql = "SELECT v FROM App\Entity\Video v WHERE v.user=:identity ORDER BY v.id DESC";
            $query = $em->createQuery($dql)->setParameter('identity', $identity->sub);

            //recoger el parámetro de pagina que viene por la URL(por get)
            $page = $request->query->getInt('page', 1);
            //items por página
            $itemsPerPage = 5;

            //llamar ala paginacion
            $pagination = $paginator->paginate($query, $page, $itemsPerPage);

            $total = $pagination->getTotalItemCount();
            //devolver datos
            $data = [
                'code' => 200,
                'status' => 'success',
                'videos' => $pagination,
                'page' => $page,
                'pages' => ceil($total / $itemsPerPage),
                'total_items' => $total,
                'user_id' => $identity->sub,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'La autenticación no es válida',
            ];
        }

        return $this->resjson($data);
    }
    
    
    //lista paginada de cualquier usuario
    
    public function userVideos(Request $request, PaginatorInterface $paginator ){
        
        //datos por defecto
        $user_id=$request->query->getInt('user');
           
        $data=[
           'status'=>'error',
           'code'=>400,
           'message'=>'No se encontraron vídeos del usuario', 
        ];
        
        $em= $this->getDoctrine()->getManager();
        $dql="SELECT v FROM App\Entity\Video v WHERE v.user=:user ORDER BY v.id DESC";
        $query= $em->createQuery($dql)->setParameter('user',$user_id);
       
        //recoger el parámetro de pagina que viene por la URL(por get)
            $page = $request->query->getInt('page', 1);
            //items por página
            $itemsPerPage = 5;

            //llamar ala paginacion
            $pagination = $paginator->paginate($query, $page, $itemsPerPage);

            $total = $pagination->getTotalItemCount();
            //devolver datos
            $data = [
                'code' => 200,
                'status' => 'success',
                'videos' => $pagination,
                'page' => $page,
                'pages' => ceil($total / $itemsPerPage),
                'total_items' => $total,
                'user_id' => $user_id,
            ];
        
        
        return $this->resjson($data);
    }
    

    //detalle de un vídeo
    public function detail(Request $request, JwtAuth $jwtauthService, $id = null) {

        //array por defecto
        $data = [
            'code' => 400,
            'status' => 'error',
            'message' => 'video no encontrado',
        ];

        //conseguir token 
        $token = $request->headers->get('Authorization', null);
        //comprobar token
        $checkToken = $jwtauthService->checkToken($token);

        if ($checkToken != false) {
            //extraemos la identidad del usuario
            $identity = $jwtauthService->checkToken($token, true);
            $user_id = $identity->sub;

            //Extarer datos del vídeo
            $video_repo = $this->getDoctrine()->getRepository(Video::class);
            $video = $video_repo->findOneBy(['id' => $id, 'user' => $user_id]);

            if (!empty($video) && is_object($video)) {
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'video' => $video,
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación no válida',
            ];
        }


        return $this->resjson($data);
    }

    //actualizar video
    public function update(Request $request, JwtAuth $jwauthService, $id = null) {

        //array por defecto
        $data = [
            'code' => 200,
            'status' => 'error',
            'message' => 'No se ha podido actualizar el video',
        ];

        //conseguir token de la cabecera
        $token = $request->headers->get('Authorization');

        //Comprobar token
        $authCheck = $jwauthService->checkToken($token);

        if ($authCheck != false) {
            //conseguir identidad del usuario
            $identity = $jwauthService->checkToken($token, true);
            $user_id = $identity->sub;


            //conseguir objeto del video con la id pasada por la URL
            $em = $this->getDoctrine()->getManager();
            $video_repo = $this->getDoctrine()->getRepository(Video::class);
            $video = $video_repo->findOneBy(["id" => $id, "user" => $user_id]);

            if (!empty($video) && is_object($video)) {

                //conseguir datos a actualizar pasados por POST
                $json = $request->get('json');
                $params = json_decode($json, null);

                if ($json != null) {
                    $title = !empty($params->title) ? $params->title : null;
                    $description = !empty($params->description) ? $params->description : null;
                    $url = !empty($params->url) ? $params->url : null;

                    if (!empty($title) && !empty($description) && !empty($url)) {
                        //setear datos a actualizar
                        $video->setTitle($title);
                        $video->setDescription($description);
                        $video->setUrl($url);
                        $video->setUpdatedAt(new \DateTime('now'));
                        //persistir y guardar
                        $em->persist($video);
                        $em->flush();

                        $data = [
                            'code' => 200,
                            'status' => 'success',
                            'video' => $video,
                        ];
                    } else {
                        $data = [
                            'code' => 400,
                            'status' => 'error',
                            'message' => 'Faltan datos',
                        ];
                    }
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación inválida',
            ];
        }


        return $this->resjson($data);
    }

    //eliminar video
    public function remove(Request $request, JwtAuth $jwtauthService, $id = null) {

        //datos por defecto
        $data = [
            'code' => '400',
            'status' => 'error',
            'message' => 'No se ha podido borrar el video',
        ];

        //conseguir token de  la cabecera
        $token = $request->headers->get('Authorization', null);
        //comprobar token
        $authCheck = $jwtauthService->checkToken($token);
        //si el token es correcto
        if ($authCheck != false) {
            //conseguir identidad de usuario
            $identity=$jwtauthService->checkToken($token,true);
            $user_id=$identity->sub;
            
            //Buscar video a borrar y ver si corresponde al usuario
            $video_repo= $this->getDoctrine()->getRepository(Video::class);
            $video=$video_repo->findOneBy(['id'=>$id,'user'=>$user_id]);
            
            //si se encuntra el video
            if(!empty($video) && is_object($video)){
                $em= $this->getDoctrine()->getManager();
                $em->remove($video);
                $em->flush();
                
                $data=[
                  'code'=>200,
                  'status'=>'success',
                  'video'=>$video
                ];
            }
        } else {
            $data = [
                'code' => '400',
                'status' => 'error',
                'message' => 'Autenticación no válida',
            ];
        }

        return $this->resjson($data);
    }
    
    //busqueda de vídeos
    public function search(Request $request){
        
        //datos por defecto
        $data=[
            'code'=>400,
            'status'=>'error',
            'message'=>'No se encontraron vídeos'
        ];
        
        $searchString=$request->get('searchString');
        
        
        //buscar videos
        $videos_repo= $this->getDoctrine()->getRepository(Video::class);
        $em=$this->getDoctrine()->getManager();
        $dql="SELECT v FROM App\Entity\Video v WHERE v.title LIKE :title OR v.description LIKE :description ORDER BY v.id DESC";
        $query=$em->createQuery($dql)->setParameter('title',"%$searchString%")->setParameter('description',"%$searchString%");
        $videos=$query->execute();
        
        if(count($videos)>0){
            $num_results=count($videos);
            $data=[
              'code'=>200,
              'status'=>'success',
              'results'=>$num_results,
              'videos'=>$videos,
            ];
        }
        
        return $this->resjson($data);
        
    }

}
