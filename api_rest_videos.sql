-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-04-2019 a las 10:38:30
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `api_rest_videos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `avatar` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password`, `role`, `created_at`, `avatar`) VALUES
(1, 'Berto', 'Ceballos', 'berto@berto.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'ROLE_ADMIN', '2019-04-10 10:00:14', '11555323360.jpeg'),
(3, 'Juan', 'perez', 'juan@juan.es', '2dea733394fbca31d32f3b791381e3d09b302af17f12366a99d4bf6bd4b88498', 'ROLE_USER', '2019-04-10 13:53:42', NULL),
(5, 'Ana', 'Rodriguez', 'ana@ana.es', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'ROLE_USER', '2019-04-10 13:56:30', NULL),
(6, 'Jose Roberto', 'Sevilla', 'jose@jose.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'ROLE_USER', '2019-04-10 13:58:08', '61555319855.jpeg'),
(7, 'Miguel', 'López', 'miguel@miguel.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'ROLE_USER', '2019-04-10 18:25:36', NULL),
(8, 'María', 'López Díaz', 'maria@maria.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'ROLE_USER', '2019-04-12 11:27:42', NULL),
(9, 'Rubén', 'Martín', 'ruben@ruben.es', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'ROLE_USER', '2019-04-12 11:29:12', NULL),
(10, 'Victor', 'Abad ', 'victor@victor.es', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'ROLE_USER', '2019-04-12 11:34:26', NULL),
(11, 'Isabel', 'Hernández', 'isabel@isabel.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'ROLE_USER', '2019-04-13 11:34:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `user_id`, `title`, `description`, `url`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Coding Challenge 3: El juego de la serpiente', 'En este desafío de Codificación de 10 minutos, intentaré codificar una versión del juego de la serpiente con p5.js ', 'https://www.youtube.com/watch?v=AaGK-fj-BAM&list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH&index=3', NULL, '2019-04-10 10:09:45', '2019-04-10 10:09:45'),
(2, 1, 'Coding Challenge #5: Space Invaders in JavaScript with p5.js', 'In this viewer submitted Coding Challenge, I take on the task of coding a Space Invaders game to play in browser in JavaScript/HTML and the p5.js library.', 'https://www.youtube.com/watch?v=biN3v3ef-Y0&list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH&index=5', NULL, '2019-04-10 10:09:45', '2019-04-10 10:09:45'),
(3, 6, 'Curso Javascript para Principiantes', 'Curso de JS desde 0', 'https://www.youtube.com/watch?v=RqQ1d1qEWlE', 'normal', '2019-04-11 12:18:12', '2019-04-11 12:18:12'),
(6, 6, 'Cursillo HTML', 'Este video es una introducción a HTML, el lenguaje que permite la creación de paginas y sitios web', 'https://www.youtube.com/watch?v=rbuYtrNUxg4', 'normal', '2019-04-11 12:30:25', '2019-04-11 17:32:01'),
(7, 1, 'Introduccion a JavaScript y jQuery', 'En este video veremos los conceptos básicos para que puedas empezar a utilizar JavaScript y jQuery sin sufrimiento', 'https://youtu.be/D31UfjthVWE', NULL, '2019-04-15 13:40:47', '2019-04-15 13:40:47'),
(8, 1, 'Crea una galería de fotos con CSS Grid', 'CSS Grid permite romper el típico (y aburrido) esquema de columnas y cambiarlo por layouts creativos y fluidos como en una galería de imagenes.', 'https://youtu.be/al760F6WAvw', NULL, '2019-04-15 13:40:47', '2019-04-15 13:40:47'),
(9, 1, 'Truco CSS: División perfecta con flexbox', 'Gracias a Flexbox tenemos elementos flexibles que peuden crecer y encogerse según nuestras necesidades, eso nos permite crear layouts como divisiones perfectas con poco esfuerzo.', 'https://youtu.be/VfXo5FMQg6s', NULL, '2019-04-15 13:43:48', '2019-04-15 13:43:48'),
(10, 1, 'Todo lo que necesitas saber de una API Restful', 'En esta primera clase del curso crea tu propia API Rest con Nodejs y MongoDB, aprenderás todos los conceptos que necesitas entender para construir tus propias APIs Restful.', 'https://youtu.be/KTugvOTfHVw', NULL, '2019-04-15 13:43:48', '2019-04-15 13:43:48'),
(11, 1, '¿Qué es el Big Data?', '¿Sabes que es el Big Data? ¿Por qué los datos son el nuevo petroleo y como su análisis y procesamiento pueden transformarse en soluciones para la ciudadanía o nuevas oportunidades para las empresas? En este video te lo contamos en detalle.', 'https://youtu.be/M26iIqmqWkI', NULL, '2019-04-15 13:48:47', '2019-04-15 13:48:47'),
(12, 1, 'Victor Robles: 6 Errores Psicológicos de los Programadores.', 'Ciertos fallos que tenemos en la mente los humanos que nos hacen caer en trampas a la hora de desarrollar aplicaciones web', 'https://youtu.be/YkKIthn9-b4', NULL, '2019-04-15 13:48:47', '2019-04-17 11:13:22');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
